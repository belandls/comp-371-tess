#version 430 core

layout(triangles, equal_spacing, ccw) in;

uniform mat4 mvp;
uniform sampler2D noiseTexture;
in vec2 tcUV[];
out vec2 teUV;
out vec4 pos;

vec2 interpolate2D(vec2 v0, vec2 v1, vec2 v2)
{
	return vec2(gl_TessCoord.x) * v0 + vec2(gl_TessCoord.y) * v1 + vec2(gl_TessCoord.z) * v2;
}

void main(void) {
	teUV = interpolate2D(tcUV[0], tcUV[1], tcUV[2]);
	vec4 posTemp = gl_TessCoord.x*gl_in[0].gl_Position + gl_TessCoord.y*gl_in[1].gl_Position + gl_TessCoord.z*gl_in[2].gl_Position;
	gl_Position = mvp * vec4(posTemp.x, posTemp.y, texture(noiseTexture, teUV).x/15.0f, posTemp.w);
	pos = gl_Position;
}