#version 430 core
out vec4 color;

uniform sampler2D noiseTexture;
uniform sampler2D normalMap;
uniform mat4 mvp;
uniform vec2 mpos;
in vec2 teUV;
in vec4 pos;

void main()
{

	vec3 cubeColour = vec3(82.0/255.0, 77.0/255.0, 63.0/255.0);
	vec3 lightColour = vec3(1.0f, 1.0f, 1.0f);
	
	//ambient lighting
	float ambientStrength = 0.15f;
    vec3 ambientContribution = ambientStrength * lightColour;
	
	//diffuse lighting
	vec3 light_position = vec3(-0.1, 0, 1.0f); //world coords

	vec3 norm = (mvp * texture(normalMap, teUV)).xyz;
	
	vec3 light_direction = normalize(light_position - pos.xyz);
	float incident_degree = max(dot(norm, light_direction), 0.0f);
	vec3 diffuseContribution = incident_degree * lightColour;
	
	vec3 resultantColour = (ambientContribution + diffuseContribution) * cubeColour;
    color = vec4(resultantColour, 1.0f);

}