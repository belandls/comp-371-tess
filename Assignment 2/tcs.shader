#version 430 core

layout(vertices = 3) out;

in vec2 vUV[];
out vec2 tcUV[];

void main(void) {
	if (gl_InvocationID == 0) {
		gl_TessLevelInner[0] = 100.0;
		gl_TessLevelOuter[0] = 100.0;
		gl_TessLevelOuter[1] = 100.0;
		gl_TessLevelOuter[2] = 100.0;
	}
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	tcUV[gl_InvocationID] = vUV[gl_InvocationID];
}