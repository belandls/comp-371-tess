//HW1 Comp 371 fall 2016
//Lab 4
//modified from http://learnopengl.com/

#include "glew.h"	// include GL Extension Wrangler
#include "glfw3.h"	// include GLFW helper library
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "Shader.h"
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"

using namespace std;

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 600;

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	std::cout << key << std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

// The MAIN function, from here we start the application and run the game loop
int main()
{
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Instancing", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	// Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
	glewExperimental = GL_TRUE;
	// Initialize GLEW to setup the OpenGL Function pointers
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	// Define the viewport dimensions
	int widthw, heightw;
	glfwGetFramebufferSize(window, &widthw, &heightw);

	glViewport(0, 0, widthw, heightw);

	Shader s;

	if (s.LoadAndCompileShaders("vertex.shader", "fragment.shader", "tcs.shader", "tes.shader")) {
		glUseProgram(s.GetCurrentProgram());
	}
	

	GLfloat vertices[] = {
		-0.5f, 0.5f, 0.0f,  
		0.5f, -0.5f, 0.0f,  
		-0.5f, -0.5f, 0.0f, 
	};

	GLfloat uvs[] = {
		0.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f
	};

	GLuint VAO, VBO, UVs_VBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);


	glGenBuffers(1, &UVs_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, UVs_VBO);
	glBufferData(GL_ARRAY_BUFFER,  sizeof(uvs), uvs, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

	glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs), remember: do NOT unbind the EBO, keep it bound to this VAO


	int width, height;
	height = 300;
	width = 300;

	float** noise = new float*[height];
	for (int i = 0; i < height; ++i) {
		noise[i] = new float[width];
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			noise[i][j] = (float)rand() / RAND_MAX;
		}
	}


	float** smoothNoise = new float*[height];
	for (int i = 0; i < height; ++i) {
		smoothNoise[i] = new float[width];
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			smoothNoise[i][j] = 0.0f;
		}
	}
	for (int k = 1; k <= 32; k *= 2) {
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				double x = i / (double)k;
				double y = j / (double)k;

				double fracX = x - int(x);
				double fracY = y - int(y);

				int x1 = (int(x) + width) % width;
				int y1 = (int(y) + height) % height;

				int x2 = (x1 + width - 1) % width;
				int y2 = (y1 + height - 1) % height;

				double value = 0.0;
				value += fracX * fracY * noise[y1][x1];
				value += (1 - fracX) * fracY * noise[y1][x2];
				value += fracX * (1 - fracY) * noise[y2][x1];
				value += (1 - fracX) * (1 - fracY) * noise[y2][x2];

				smoothNoise[i][j] += (k / 32.0) *value;
			}
		}
	}

	float* sinus = new float[height * width*3];
	glm::vec3* norms = new glm::vec3[height * width];
	/*for (int i = 0; i < height; ++i) {
		sinus[i] = new float[width];
	}*/
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			sinus[i*width*3 + (j*3)] = 1.0f;
			sinus[i*width*3 + (j * 3) + 1] = 1.0f;
			sinus[i*width*3 + (j * 3) + 2] = 1.0f;
		}
	}
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			double test = j*0.0 / width + i * 10.0 / height + 0.6 * smoothNoise[i][j];
			sinus[i*width*3 + (j * 3)] = 1.0f - fabs(sin(test * 3.14159));
			sinus[i*width*3 + (j * 3) + 1] = 1.0f - fabs(sin(test * 3.14159));
			sinus[i*width*3 + (j * 3) + 2] = 1.0f - fabs(sin(test * 3.14159));
		}
	}

	for (float y = 0; y < height; ++y) {
		for (float x = 0; x < width; ++x) {
			float nextX, nextY;
			if (x == width - 1){
				nextX = x - 1;
			}
			else{
				nextX = x + 1;
			}
			if (y == height - 1){
				nextY = y - 1;
			}
			else{
				nextY = y + 1;
			}
			glm::vec3 current(x / width, y / height, sinus[(int)y*width * 3 + ((int)x * 3)]);
			glm::vec3 nextXPoint(nextX / width, y / height, sinus[(int)y*width * 3 + ((int)nextX * 3)]);
			glm::vec3 nextYPoint(x / width, nextY / height, sinus[(int)nextY*width * 3 + ((int)x * 3)]);

			glm::vec3 v1 = nextXPoint - current;
			glm::vec3 v2 = nextYPoint - current;
			norms[(int)y*width + (int)x] = glm::normalize(glm::cross(v1, v2)) ;
		}
	}

	glActiveTexture(GL_TEXTURE0); //select texture unit 0

	GLuint cube_texture;
	glGenTextures(1, &cube_texture);
	glBindTexture(GL_TEXTURE_2D, cube_texture); //bind this texture to the currently bound texture unit

												// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Load image, create texture and generate mipmaps
	int cube_texture_width, cube_texture_height;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, sinus);

	glActiveTexture(GL_TEXTURE1); //select texture unit 1

	GLuint normal_map;
	glGenTextures(1, &normal_map);
	glBindTexture(GL_TEXTURE_2D, normal_map); //bind this texture to the currently bound texture unit

	// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);

	// Set texture wrapping to GL_REPEAT (usually basic wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_FLOAT, norms);

	glm::mat4 mvp;
	//mvp = glm::rotate(mvp, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	//mvp = glm::rotate(mvp, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	//mvp = glm::rotate(mvp, glm::radians(45.0f), glm::vec3(0.0f, 1.0f, 1.0f));
	GLint mvpPos = glGetUniformLocation(s.GetCurrentProgram(), "mvp");
	glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(mvp));
						  // Game loop

	glEnable(GL_DEPTH_TEST);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		// Render
		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//mvp = glm::rotate(glm::mat4(), glm::radians(180.f), glm::vec3(1.0f, 1.0f, 0.0f));
		//mvp = glm::rotate(glm::mat4(), glm::radians((float)glfwGetTime() * 30), glm::vec3(1.0f, 1.0f, 0.0f));
		//mvp = glm::rotate(mvp, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		//mvp = glm::rotate(mvp, glm::radians(45.0f), glm::vec3(0.0f, 1.0f, 1.0f));
		GLint mvpPos = glGetUniformLocation(s.GetCurrentProgram(), "mvp");
		glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(mvp));



		double w, h;
		float ww, hh;
		glfwGetCursorPos(window, &w, &h);
		ww = (w / 800.0f) *2.0f - 1.f;
		hh = ((600.0f-h) / 600.0f) *2.0f - 1.f;

		cout << ww << "," << hh << endl;

		glUniform1i(glGetUniformLocation(s.GetCurrentProgram(), "noiseTexture"), 0);
		glUniform1i(glGetUniformLocation(s.GetCurrentProgram(), "normalMap"), 1);
		glUniform2fv(glGetUniformLocation(s.GetCurrentProgram(), "mpos"),1, (GLfloat*)(&glm::vec2(ww, hh)));

		glBindVertexArray(VAO);
		glDrawArrays(GL_PATCHES, 0, 3);
		glBindVertexArray(0);

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

